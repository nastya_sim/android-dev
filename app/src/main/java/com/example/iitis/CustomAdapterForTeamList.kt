package com.example.iitis

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.team.view.*
import android.widget.Toast


class CustomAdapterForTeamList(var context: Context, private val itemClickListener: ItemClickListener) :
    RecyclerView.Adapter<CustomAdapterForTeamList.ViewHolder>() {
    var teamList = TeamListMaxInfo()

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(holder: ViewGroup, position: Int): ViewHolder {
        return ViewHolder(context, LayoutInflater.from(context).inflate(R.layout.team, holder, false))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return teamList.teams.size
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(teamList.teams[position], itemClickListener)
    }

    //the class is hodling the list view
    class ViewHolder(var context: Context, private val view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(team: FullInfoAboutTeam, itemClickListener: CustomAdapterForTeamList.ItemClickListener) {
            view.viewOneTeam.text = team.name
            var db = DataBaseHandler(context)
            var check = false
            check = db.checkValue(team.id)
            val checkValue = if (check) "true" else "false"
            Log.d("MyDB", "isCheck = $checkValue +  id = ${team.id}")
            itemView.plus.setBackgroundResource(R.drawable.like)
            itemView.plus.setBackgroundResource(
                if (check) {
                    R.drawable.love
                } else {
                    R.drawable.like
                }
            )

            view.plus.setOnClickListener {
                check = db.checkValue(team.id)
                if (!check) {
                    var value = Team(
                        team.id,
                        team.name,
                        team.locationName,
                        team.firstYearOfPlay,
                        team.conference.name,
                        team.officialSiteUrl
                    )
                    db.insertData(value)
                    itemView.plus.setBackgroundResource(R.drawable.love)
                    Log.d("MyDB", "Add item = $checkValue +  id = ${team.id}")
                } else {
                    db.deleteData(team.id)
                    itemView.plus.setBackgroundResource(R.drawable.like)
                    Toast.makeText(context, "Deleted from favourite", Toast.LENGTH_SHORT).show()
                    Log.d("MyDB", "Delete item = $checkValue +  id = ${team.id}")
                }
            }
            view.setOnClickListener { itemClickListener.onItemClick(team) }
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: FullInfoAboutTeam) {
        }
    }

    fun setTeams(teams: TeamListMaxInfo) {
        this.teamList = teams
        notifyDataSetChanged()
    }

}
