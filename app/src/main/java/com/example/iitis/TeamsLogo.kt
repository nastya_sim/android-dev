package com.example.iitis

import com.google.gson.annotations.SerializedName

class Logotype(
    val id: Int,
    val url: String
)

class TeamsLogo(
    @SerializedName("teams")
    val teams: ArrayList<Logotype> = ArrayList()
)