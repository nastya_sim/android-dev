package com.example.iitis

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.system.Os.close
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.lang.Exception
import java.lang.ref.WeakReference
import java.nio.charset.Charset
import java.util.*


class activity_two : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        val jsonListLogos = "{\"teams\" :[\n" +
                "{\"id\" : 8,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/MTL_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 41,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/MWN_19171918_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 45,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/SLE_19341935_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 37,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/QBD_19191920_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 10,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/TOR_19381939-19621963_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 6,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/BOS_alt.svg\"\n" +
                "},\n" +
                "{\"id\" : 43,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/MMR_19241925_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 51,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/BRK_19411942_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 39,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/QUA_19301931_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 3,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/NYR_19521953-19661967_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 16,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/CHI_19891990-19951996_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 17,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/DET_19321933-19471948_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 49,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/CGS_19701971-19731974_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 26,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/LAK_20112012-20182019_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 25,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/DAL_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 4,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/PHI_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 5,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/PIT_19721973-19911992_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 19,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/STL_19891990-19971998_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 7,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/BUF_19961997-19981999_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 23,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/VAN_20072008-20182019_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 20,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/AFM_19721973-19791980_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 2,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/NYI_20102011-20162017_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 1,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/NJD_19821983-19911992_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 15,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/WSH_19951996-20062007_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 22,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/EDM_20112012-20162017_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 12,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/CAR_19992000-20122013_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 21,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/COL_19951996-19981999_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 53,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/ARI_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 28,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/SJS_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 9,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/OTT_19971998-20062007_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 14,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/TBL_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 24,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/ANA_20062007-20122013_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 13,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/FLA_19992000-20152016_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 18,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/NSH_19981999-20102011_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 52,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/WPG_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 29,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/CBJ_light.svg\"\n" +
                "},\n" +
                "{\"id\" : 30,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/MIN_dark.svg\"\n" +
                "},\n" +
                "{\"id\" : 54,\n" +
                "\"url\":\"https://assets.nhle.com/logos/nhl/svg/VGK_light.svg\"\n" +
                "}]\n" +
                "}"

        val gson = Gson()
        val arrayTutorialType = object : TypeToken<TeamsLogo>() {}.type
        var logos: TeamsLogo = gson.fromJson(jsonListLogos, arrayTutorialType)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_two)
        val id = intent.getStringExtra("id")
        val name = intent.getStringExtra("name")
        val city = intent.getStringExtra("city")
        val conf = intent.getStringExtra("conf")
        val year = intent.getStringExtra("year")
        val url = intent.getStringExtra("url")

        (findViewById(R.id.name_of_team) as TextView).text = name
        (findViewById(R.id.about_team) as TextView).text = city + ", " + conf + ", " + year

        var buttonUrl = (findViewById(R.id.link) as Button)
        buttonUrl.setOnClickListener {
            var i = Intent(Intent.ACTION_VIEW)
            i.setData(Uri.parse(url))
            startActivity(i)
        }
        var logotype = seerchLogoById(logos, id.toInt())

        Glide.with(this)
            .load(logotype)
            .into(logo)

        val adapter = CustomAdapterForPlayerList(this)

        val service: GetTeamService = RetrofitClientInstance().getRetrofitInstance().create(GetTeamService::class.java)
        val call: Call<PlayerList> = service.getAllPlayers(id)
        call.enqueue(object : Callback<PlayerList> {
            override fun onFailure(call: Call<PlayerList>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<PlayerList>?, response: Response<PlayerList>?) {
                response?.body()?.let { adapter.setPlayers(it) }

                recycler.apply {
                    layoutManager = LinearLayoutManager(this@activity_two)
                    this.adapter = adapter
                }
            }
        })

        val mBtnShare = findViewById<Button>(R.id.share)

        mBtnShare.setOnClickListener {

            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, url)
                type = "text/plain"
            }
            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

    }

    fun seerchLogoById(logs: TeamsLogo, item: Int): String {
        for (value in logs.teams) {
            if (value.id == item)
                return value.url
        }
        return "logo not found!"
    }
}
