package com.example.iitis


import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    var mFirebaseAnalytics: FirebaseAnalytics? = null
    override fun onCreate(savedInstanceState: Bundle?) {

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (findViewById(R.id.favourite) as Button).setOnClickListener {
            val intent = Intent(this@MainActivity, favourites::class.java)
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "click on favourites")
            startActivity(intent)
        }

        val adapter = CustomAdapterForTeamList(this, object : CustomAdapterForTeamList.ItemClickListener {
            override fun onItemClick(item: FullInfoAboutTeam) {

                val intent = Intent(this@MainActivity, activity_two::class.java)
                intent.putExtra("id", item.id.toString())
                intent.putExtra("name", item.name)
                intent.putExtra("city", item.locationName)
                intent.putExtra("conf", item.conference.name)
                intent.putExtra("year", item.firstYearOfPlay.toString())
                intent.putExtra("url", item.officialSiteUrl)
                startActivity(intent)

            }
        })


        val service: GetTeamService = RetrofitClientInstance().getRetrofitInstance().create(GetTeamService::class.java)
        val call: Call<TeamListMaxInfo> = service.getAllTeams()
        call.enqueue(object : Callback<TeamListMaxInfo> {
            override fun onFailure(call: Call<TeamListMaxInfo>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<TeamListMaxInfo>?, response: Response<TeamListMaxInfo>?) {
                response?.body()?.let { adapter.setTeams(it) }
                recycler.apply {
                    layoutManager = LinearLayoutManager(this@MainActivity)
                    this.adapter = adapter
                }
            }
        })
    }
}
