package com.example.iitis

import com.google.gson.annotations.SerializedName

class TeamListMaxInfo(
    @SerializedName("teams")
    val teams: ArrayList<FullInfoAboutTeam> = ArrayList()
)

class TeamListMinInfo(
    val teams: ArrayList<Team> = ArrayList()
)