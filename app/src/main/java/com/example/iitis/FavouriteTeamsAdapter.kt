package com.example.iitis


import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.team.view.*


class FavouriteTeamsAdapter(
    var context: Context,
    val teamsList: ArrayList<Team>,
    private val itemClickListener: FavouriteTeamsAdapter.ItemClickListener
) : RecyclerView.Adapter<FavouriteTeamsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteTeamsAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.dislike, parent, false)
        return ViewHolder(context, v)
    }

    override fun onBindViewHolder(holder: FavouriteTeamsAdapter.ViewHolder, position: Int) {
        holder.onBind(teamsList[position], itemClickListener)
    }

    override fun getItemCount(): Int {
        return teamsList.size
    }

    class ViewHolder(var context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(team: Team, itemClickListener: FavouriteTeamsAdapter.ItemClickListener) {
            itemView.viewOneTeam.text = team.name

            itemView.plus.setOnClickListener {
                var value = Team(
                    team.id,
                    team.name,
                    team.locationName,
                    team.firstYearOfPlay,
                    team.conference,
                    team.officialSiteUrl
                )
                var db = DataBaseHandler(context)
                db.deleteData(value.id)

                itemView.viewOneTeam.apply {
                    paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                }
                itemView.plus.background = (null)
            }

            itemView.setOnClickListener { itemClickListener.onItemClick(team) }
        }
    }

    interface ItemClickListener {
        fun onItemClick(item: Team) {
        }
    }

}