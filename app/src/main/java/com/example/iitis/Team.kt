package com.example.iitis

class Team(
    val id: Int,
    val name: String,
    val locationName: String,
    val firstYearOfPlay: Int,
    val conference: String,
    val officialSiteUrl: String
)