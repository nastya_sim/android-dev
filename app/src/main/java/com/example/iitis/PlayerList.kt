package com.example.iitis

import com.google.gson.annotations.SerializedName

class Person(
    val id: Int,
    val fullName: String,
    val link: String
)

class Position(
    val code: String,
    val name: String,
    val type: String,
    val abbreviation: String
)

class Player(
    val person: Person,
    val jerseyNumber: Int,
    val position: Position
)


class PlayerList(
    @SerializedName("roster")
    val roster: ArrayList<Player> = ArrayList(),
    @SerializedName("link")
    val link: String = String()
)