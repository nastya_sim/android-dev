package com.example.iitis

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_favourites.*

class favourites : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourites)

        var context = this
        var db = DataBaseHandler(context)
        var data = db.readData()
        var teams = ArrayList<Team>()
        for (i in 0..(data.size - 1)) {
            var value = Team(
                data.get(i).id,
                data.get(i).name,
                data.get(i).locationName,
                data.get(i).firstYearOfPlay,
                data.get(i).conference,
                data.get(i).officialSiteUrl
            )
            teams.add(value)
        }

        val obj_adapter = FavouriteTeamsAdapter(this, teams, object : FavouriteTeamsAdapter.ItemClickListener {
            override fun onItemClick(item: Team) {

                val intent = Intent(this@favourites, activity_two::class.java)
                intent.putExtra("id", item.id.toString())
                intent.putExtra("name", item.name)
                intent.putExtra("city", item.locationName)
                intent.putExtra("conf", item.conference)
                intent.putExtra("year", item.firstYearOfPlay.toString())
                intent.putExtra("url", item.officialSiteUrl)
                startActivity(intent)
            }
        })

        recycler.apply {
            layoutManager = LinearLayoutManager(this@favourites)
            this.adapter = obj_adapter
        }

    }

    override fun onBackPressed() {
        val intent = Intent(this@favourites, MainActivity::class.java)
        startActivity(intent)
    }
}
