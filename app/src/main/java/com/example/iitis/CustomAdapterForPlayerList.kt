package com.example.iitis

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.team.view.*


class CustomAdapterForPlayerList(val context: Context) : RecyclerView.Adapter<CustomAdapterForPlayerList.ViewHolder>() {

    var playerList: PlayerList = PlayerList()

    override fun onCreateViewHolder(holder: ViewGroup, position: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.player, holder, false))
    }

    override fun getItemCount(): Int {
        return playerList.roster.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(playerList.roster[position])
    }

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(player: Player) {
            view.viewOneTeam.text = player.person.fullName + " (" + player.jerseyNumber + ")"
        }
    }

    fun setPlayers(players: PlayerList) {
        this.playerList = players
        notifyDataSetChanged()
    }

}
