package com.example.iitis

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

val DATABASE_NAME = "TeamsDB"
val TABLE_NAME = "Teams"
val COL_ID_TEAM = "id"
val COL_NAME = "name"
val COL_URL = "officialSiteUrl"
val COL_CONFERENCE = "conference"
val COL_CITY = "locationName"
val COL_YEAR = "firstYearOfPlay"

class DataBaseHandler(val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, 1) {
    override fun onCreate(p0: SQLiteDatabase?) {
        val createTable = "create table " + TABLE_NAME + " (" +
                COL_ID_TEAM + " integer primary key, " +
                COL_NAME + " varchar(256), " +
                COL_URL + " varchar(256), " +
                COL_CONFERENCE + " varchar(256), " +
                COL_CITY + " varchar(256), " +
                COL_YEAR + " integer)"
        p0?.execSQL(createTable)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
    }

    fun insertData(team: Team) {
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_ID_TEAM, team.id)
        cv.put(COL_NAME, team.name)
        cv.put(COL_URL, team.officialSiteUrl)
        cv.put(COL_CONFERENCE, team.conference)
        cv.put(COL_CITY, team.locationName)
        cv.put(COL_YEAR, team.firstYearOfPlay)
        var result = db.insert(TABLE_NAME, null, cv)
        if (result == -1.toLong())
            Toast.makeText(context, "Already added", Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(context, "Add to favourite", Toast.LENGTH_SHORT).show()

    }

    fun readData(): MutableList<Team> {
        var list: MutableList<Team> = ArrayList()

        var db = this.readableDatabase
        val query = "Select * from " + TABLE_NAME
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                var team = Team(
                    result.getString(result.getColumnIndex(COL_ID_TEAM)).toInt(),
                    result.getString(result.getColumnIndex(COL_NAME)),
                    result.getString(result.getColumnIndex(COL_CITY)),
                    result.getString(result.getColumnIndex(COL_YEAR)).toInt(),
                    result.getString(result.getColumnIndex(COL_CONFERENCE)),
                    result.getString(result.getColumnIndex(COL_URL))
                )
                list.add(team)
            } while (result.moveToNext())
        }
        result.close()
        return list
    }

    fun deleteData(id: Int) {
        val db = this.writableDatabase
        db.delete(TABLE_NAME, "id=" + id, null)
        db.close()
    }

    fun checkValue(id: Int): Boolean {
        val db = this.writableDatabase

        val query = "Select * from " + TABLE_NAME + " where " + COL_ID_TEAM + "=" + id
        val result = db.rawQuery(query, null)
        if (result.count > 0) {
            result.close()
            return true
        }

        result.close()
        return false
    }

}
