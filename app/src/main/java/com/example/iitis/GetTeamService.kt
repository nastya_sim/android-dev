package com.example.iitis

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GetTeamService {

    @GET("/api/v1/teams")
    fun getAllTeams(): Call<TeamListMaxInfo>

    @GET("/api/v1/teams/{id}/roster")
    fun getAllPlayers(@Path("id") id: String): Call<PlayerList>

}