package com.example.iitis

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClientInstance() {

    lateinit var retrofit: Retrofit
    val BASE_URL: String = "https://statsapi.web.nhl.com/"

    fun getRetrofitInstance(): Retrofit {
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit
    }

}