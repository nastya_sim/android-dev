package com.example.iitis

class Division(
    val id: Int,
    val name: String,
    val nameShort: String,
    val link: String,
    val abbreviation: String
)

class Conference(
    val id: Int,
    val name: String,
    val link: String
)

class Franchise(
    val franchiseId: Int,
    val teamName: String,
    val link: String
)

class TimeZone(
    val id: String,
    val offset: Int,
    val tz: String
)

class Venue(
    val name: String,
    val link: String,
    val city: String,
    val timeZone: TimeZone
)

class FullInfoAboutTeam(
    val id: Int,
    val name: String,
    val link: String,
    val venue: Venue,
    val abbreviation: String,
    val teamName: String,
    val locationName: String,
    val firstYearOfPlay: Int,
    val division: Division,
    val conference: Conference,
    val franchise: Franchise,
    val shortName: String,
    val officialSiteUrl: String,
    val franchisedId: Int,
    val active: Boolean

)